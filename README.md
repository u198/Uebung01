# Uebung01

## Zu finden auf: (https://gitlab.com/u198/Uebung01)

### Objektorientiert zu Programmieren ist missglückt, da ich noch ein bisschen mehr lernen muss, aber die Funktionen geben das aus was sie sollen ^^ (wird überarbeitet)

## Funktionen

Die Main Methode ist ReaderMain.java

Der User wird gebeten einen Path des Ordners auszuwählen, welcher eingelesen werden soll(zum Beispiel: C:\Beispiel) 

##### Liest nur XML Dateien ein!

Danach öffnet sich ein Menü, welches man bedient indem man einen Integer eintippt und Enter drückt

Es gibt die Optionen:
1. Auflistung aller Redner
2. Einen bestimmten Redner auszugeben (Groß- und Kleinschreibung wichtig)
3. Alle Redner, welche Mitglieder einer bestimmten Partei sind, auszugeben (Groß- und Kleinschreibung wichtig, Und Vermeidung von Artikeln: statt "DIE LINKE" soll "LINKE" eingegeben werden)
4. Ausgabe des Textes eines Tagesordnungspunktes gemäßt Situngs- und Nummern-Index
5. Das Programm zu beenden
