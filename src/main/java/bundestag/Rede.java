package bundestag;

public class Rede {
    String Inhalt;
    Abgeordnete Redner;
    String Kommentare;

    public String getInhalt() {
        return Inhalt;
    }

    public Abgeordnete getRedner() {
        return Redner;
    }

    public String getKommentare() {
        return Kommentare;
    }

}
