package bundestag;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProcessComments {
    public static List ProcessComments(File xmlFile, List Entities) {       // Wird aktuell nicht benötigt | Verständnis für Filter
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList Name = doc.getElementsByTagName("kommentar");
            List<String> kom = new ArrayList<>();
            for (int itr = 0; itr < Name.getLength(); itr++) {
                Node node = Name.item(itr);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    String x = eElement.getTextContent();
                    Pattern pattern = Pattern.compile("\\(?\\((.*?)\\)\\)?");
                    Matcher matcher = pattern.matcher(x);
                    while (matcher.find()) {
                        kom.add(matcher.group(1));
                    }
                }
            } return kom;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
