package bundestag;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

public class ReaderMain {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Geben sie den Path der zu lesenden Dateien ein: ");
        String XmlUser = scanner.nextLine();
        while (true) { // endless loop damit man bei falscher Eingabe oder nachdem man mit einer option fertig ist, danach etwas anderes machen kann
            System.out.println("Select an option by inputting an Integer: \n" +
                    "1: Auflistung aller Redner\n" +
                    "2: Auflistung eines bestimmten Abgeordneten\n" +
                    "3: Auflistung aller Redner nach Partei/Fraktion\n" +
                    "4: Ausgabe des Textes eines Tagesordnungspunktes gemäßt Situngs- und Nummern-Index\n" +
                    "5: exit");
            int selected = scanner.nextInt();

            switch (selected) {
                case 1:
                    read(XmlUser, selected, null, null, null, null, null); // liest die Datei und es wird der Input mitgegeben
                    break;
                case 2:
                    System.out.println("Geben sie den Vornamen an");
                    String vorname = scanner.next();
                    System.out.println("Geben sie den Nachnamen an");
                    String nachname = scanner.next();
                    read(XmlUser, selected, vorname, nachname, null, null, null); // liest die Datei und es wird der Input und name mitgegeben
                    break;
                case 3:
                    System.out.println("Geben sie die Partei oder Fraktion an");
                    String partei = scanner.next();
                    read(XmlUser, selected, null, null, partei, null, null); // liest die Datei und es wird der Input und die Partei mitgegeben
                    break;
                case 4:
                    System.out.println("Geben sie die Sitzungsnummer an");
                    String sitzung = scanner.next();
                    System.out.println("Geben sie den Nummern-index an");
                    String nummer = scanner.next();
                    read(XmlUser, selected, null, null, null, sitzung, nummer); // liest die Datei und es wird der Input und die Sitzungs-Nummerindex mitgegeben
                    break;
                case 5: // verlässt das Programm | beendet den loop
                    System.out.println("Tschüssiii");
                    System.exit(0);
                default: // falls keiner der cases erfüllt wird
                    System.out.println("Invalid input\n");
            }
        }
    }

    public static void read(String Xml, int select, String vor, String nach, String partei, String sitzung, String nummer) { // Parameter Übergabe ist Katastrophe
        File xmlFile = new File(Xml);
        File[] files = xmlFile.listFiles();
        for (File file : files) {
            if (file.isFile() && file.getName().endsWith(".xml")) { // liest alle Dateien die auf .xml enden
                switch (select) {
                    case 1:
                        processName(file);
                        break;
                    case 2:
                        filteredName(file, vor, nach);
                        break;
                    case 3:
                        filteredPartei(file, partei);
                        break;
                    case 4:
                        filteredTO(file, sitzung, nummer);
                        break;
                }
            }
        }
    }
    public static void processName(File xmlFile) { // Die Methode gibt alle Namen aus
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList Name = doc.getElementsByTagName("rednerliste");
            ArrayList<String> names = new ArrayList<>();
            for (int itr = 0; itr < Name.getLength(); itr++) {
                Node node = Name.item(itr);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    String x = eElement.getTextContent();
                    names.add(x);
                    System.out.println(names);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void filteredName(File xmlFile, String vor, String nach) { // Die Methode gibt einen Namen so oft aus wie oft er in der Datei vorkommt ( kann man mit einem Filter verbessern )
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList Name = doc.getElementsByTagName("redner");
            for (int itr = 0; itr < Name.getLength(); itr++) {
                Node node = Name.item(itr);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    String fName = eElement.getTextContent();
                    if (fName.contains(vor) && fName.contains(nach)) {
                        System.out.println(fName);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void filteredPartei(File xmlFile, String partei) { // Die Methode gibt alle Redner in einer gewählten Partei aus
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList Name = doc.getElementsByTagName("redner");
            for (int itr = 0; itr < Name.getLength(); itr++) {
                Node node = Name.item(itr);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    String fPartei = eElement.getTextContent();
                    if (fPartei.contains(partei)) {
                        System.out.println(fPartei);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void filteredTO(File xmlFile, String Sitzung, String Nummer) { // Die Methode gibt den Text eines gewählten Tagesordnungspunkt anhand der Sitzungsnummer + Nummernindex
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList proto = doc.getElementsByTagName("dbtplenarprotokoll");
            NodeList TO = doc.getElementsByTagName("tagesordnungspunkt");
            for (int itr = 0; itr < TO.getLength(); itr++) { // sucht solange alle Dateien durch bis es einen match gibt
                Node node = proto.item(0);
                Node node2 = TO.item(itr);
                if (node2.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    String sitz = eElement.getAttribute("sitzung-nr");
                    if (sitz.equals(Sitzung)) {
                        Element eElement2 = (Element) node2;
                        String num = eElement2.getAttribute("top-id");
                        if (num.contains("Tagesordnungspunkt" + " "+ Nummer)) {
                            System.out.println("Tagesordnungspunkt: " + Nummer + eElement2.getTextContent());
                            break;
                        }
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}