package bundestag;

public class Tagesordnungspunkt {
    Rede Reden;

    public Rede getReden() {
        return Reden;
    }

    public String getKommentare() {
        return Kommentare;
    }

    public int getNummern_index() {
        return Nummern_index;
    }

    public void setNummern_index(int nummern_index) {
        Nummern_index = nummern_index;
    }

    String Kommentare;
    int Nummern_index;
}
