package bundestag;

public class Protokoll {
    String Datum;
    String Sitzungsnummer;
    String Legislaturperiode;
    String Titel;
    Tagesordnungspunkt TOs;
    String Sitzungsleiter;

    public String getSitzungsnummer() {
        return Sitzungsnummer;
    }

    public String getLegislaturperiode() {
        return Legislaturperiode;
    }

    public String getTitel() {
        return Titel;
    }

    public Tagesordnungspunkt getTOs() {
        return TOs;
    }

    public String getSitzungsleiter() {
        return Sitzungsleiter;
    }

    public String getDatum() {
        return Datum;
    }
}
